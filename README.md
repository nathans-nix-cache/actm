# actm

Tiny actors library for rust.

Supports both asynchronous (executor agnostic) and synchronous actors, and sync/async actors can be
mixed within the same program.

This project is licensed under the terms of the [The Zero Clause BSD License](./LICENSE).

## Contributing

See [`contributing.md`](./doc/contributing.md) as well as our
[project hub](https://sr.ht/~thatonelutenist/actm).
